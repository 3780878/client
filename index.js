import React from "react";
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import { Provider } from 'react-redux';/*main component for use store*/
import { createStore } from 'redux';/*function for create store*/

import reducer from './src/reducers';/*store objects objectu hranilisha, na kajdui komponent sozdaetsya svoi reducer*/

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

import App from "./src/App";

ReactDOM.render(

    <Provider store = {store}>
        <BrowserRouter>
            <App />
        </BrowserRouter> 
    </Provider> ,document.getElementById("app")
);