import React from "react";
import { connect } from "react-redux";
import {removeProductAction, showCartAction} from "../../actions/app";

const CartProduct = (props) =>{

    let closeCart = ()=>{
        setTimeout(()=>{
            props.showCart();
        }, 5);
    }

    let remove = () => {
        let data = {...props.data, close:closeCart}
        props.removeProduct(data);
    }


        return ( 
            <tr>
            <td><img className="img-cart" src={props.data.image} /></td>
            <td>{props.data.name}</td>
            <td>{props.data.quantity}</td>
            <td>{props.data.sum}</td>
            <td><i onClick={remove} className="fa fa-trash" aria-hidden="true"></i></td>
        </tr>
        )
    }


    export default connect (
        state => ({

        totalQuantity:state.cart.totalQuantity

        }),
        dispatch => ({
            removeProduct: (d)=>{
                removeProductAction(dispatch, d);
            },
            showCart: ()=>{

                showCartAction(dispatch);
                
            }
        })

    )(CartProduct);
