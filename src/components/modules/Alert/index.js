import React from "react";

import {sendProductsAction, changeCustomerAction, changeCustomerErrorsAction} from "../../../actions/app";

import { connect } from "react-redux";

import './style.scss';



const Alert = (props) => {

    

    let send = ()=>{
        props.sendProducts({products: props.products, customer: props.customer});
    }
    let change = (el)=>{
        var data = {};
        
        var val = el.target.value;
        var name = el.target.name;

        data[name] = val;

        if(name === 'phone' && !val.match(/^(\+380|380|80|0){1}[0-9]{9}$/)){
            props.changeCustomerErrors({'phone':true});
        }else if(name === 'phone'){
            props.changeCustomerErrors({'phone':false});
        }

        if(name === 'name' && !val.match(/^[\wа-яА-Я]{2,25}$/)){
            props.changeCustomerErrors({'name':true});
        }else if(name === 'name'){
            props.changeCustomerErrors({'name':false});
        }
        props.changeCustomer(data);
    }

        return ( 
        <div className="alert-cart">
           <form>
               <label>
                   Enter phone number:<br/>
                   {
                   props.customerErrors.phone ?
                    <span className="text-danger">Not valid phone, example: +380000000000 </span>
                    :''
                   }
               <input name="phone" onChange={change} className="form-control"/>
              
               </label>
               <label>
                   Enter your name:
               <input name="name"  onChange={change} className="form-control"/>
               {
                   props.customerErrors.name ?
                        <span className="text-danger">Are you sure? HAHA I can't believe what that is your name! name must be: a-z, max 50 characters</span>
                    :''
                }
               </label>
               <div className="form-group">
                 <button onClick={send} type="button" className="btn btn-info">Confirm your order </button>
               </div>
               {
                props.message ?
                <div className="alert alert-info" role="alert">
                    Products will be your's!
                </div>
                : ''

               }
           </form>
        </div>
        )
    }


export default connect(
    state => ({
        products:state.cart.products,
        message:state.cart.messageSuccess,
        customer:state.app.customer,
        customerErrors:state.app.customerErrors

    }),

    dispatch => ({/*fynciya dlya obnovleniua redux*/
        sendProducts:(d)=>{
            sendProductsAction(d, dispatch);
        },
        changeCustomer:(d)=>{
            changeCustomerAction(d, dispatch);
        },
        changeCustomerErrors:(d)=>{
            changeCustomerErrorsAction(d, dispatch);
        }
    })
)(Alert);
