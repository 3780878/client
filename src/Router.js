import React from 'react';

import { Route, Switch } from 'react-router-dom';

import Home from './components/pages/Home';
import Login from './components/pages/Login';
import Products from './components/pages/Products';
import Product from './components/pages/Product';
import Registration from './components/pages/Registraton';
import Contacts from './components/pages/Contacts';

export default (props) => (
    <Switch>
        <Route exact path='/' component={Home}/>
        <Route path='/login' component={Login}/>
        <Route path='/products' component={Products}/>
        <Route path='/product/:slug' component={Product}/>
        <Route path='/registration' component={Registration}/>
        <Route path='/contacts' component={Contacts}/>
    </Switch>
);