
const initState = {
    products: [],
    showCart: false,
    showAlert: false,
    customer:{
        name:'',
        phone:''
    },
    customerErrors:{
        name: false,
        phone: false
    }
}

export default function app(state = initState, action)
{
    switch(action.type)
    {
        case "SHOW_CART":

        return {
            ...state,
            showCart: !state.showCart,/*menyaem false na true*/
            ...action.data
        }
        case "SET_PRODUCTS":

        return {
            ...state,
            products:[...action.data]            
        }
        case "SHOW_ALERT":

        return {
            ...state,
            showAlert:!state.showAlert           
        }
        case "CHANGE_CUSTOMER":

        return {
            ...state,
            customer:{...state.customer, ...action.data}          
        }
        case "CHANGE_CUSTOMER_ERRORS":

        return {
            ...state,
            customerErrors:{...state.customerErrors, ...action.data}          
        }
    }
    return state;
}